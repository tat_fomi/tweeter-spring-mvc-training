package ua.rd.repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import ua.rd.domain.Tweet;
import ua.rd.ioc.Benchmark;

@Repository("tweetRepository")
@Qualifier("tweetRepository1")
public class InMemTweetRepository implements TweetRepository {

    private List<Tweet> tweets;

    @PostConstruct
    public void init() {
        tweets = new ArrayList<>(
                Arrays.asList(
                        new Tweet("Andrii", "First tweet"),
                        new Tweet("Serg", "Second tweet")
                ));
    }

    public InMemTweetRepository() {
    }

    public InMemTweetRepository(List<Tweet> tweets) {
        this.tweets = tweets;
    }

    @Override
    @Benchmark
    public Iterable<Tweet> getAllTweets() {
        return tweets;
    }

    @Override
    public void save(Tweet tweet) {
        tweets.add(tweet);
    }

}
