package ua.rd;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import ua.rd.domain.Tweet;
import ua.rd.ioc.RepoConfig;
import ua.rd.ioc.ServiceConfig;
import ua.rd.service.TweetService;

import java.util.Arrays;

public class AnnotationSpringIoCRunner {

    public static void main(String[] args) {

        ConfigurableApplicationContext repoCtx = new AnnotationConfigApplicationContext(RepoConfig.class);
        AnnotationConfigApplicationContext serviceCtx = new AnnotationConfigApplicationContext();
        serviceCtx.register(ServiceConfig.class);
        serviceCtx.setParent(repoCtx);

        ConfigurableEnvironment environment = serviceCtx.getEnvironment();
        environment.setActiveProfiles("test");
        System.out.println(repoCtx.getBeanFactory().getBeanDefinition("tweetRepository"));
        serviceCtx.refresh();

        TweetService service = (TweetService) serviceCtx.getBean("tweetService");
        System.out.println(service.getAllTweets());
    }
}
