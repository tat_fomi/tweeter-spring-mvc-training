package ua.rd.web.infrastructure;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author andrii
 */
public class BeanNameURLHandlerMapping implements HandlerMapping {
    @Override
    public String beanNameFromRequest(HttpServletRequest request){
        String requestURL = request.getRequestURI();
        String beanName = requestURL.substring(requestURL.lastIndexOf('/'));
        return beanName;
    }
}
