package ua.rd.web.infrastructure;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author andrii
 */
public interface HandlerMapping {

    String beanNameFromRequest(HttpServletRequest request);
    
}
