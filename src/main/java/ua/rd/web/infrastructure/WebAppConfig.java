package ua.rd.web.infrastructure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.handler.BeanNameUrlHandlerMapping;
import ua.rd.service.TweetService;

@Configuration
//@ComponentScan(basePackages = "ua.rd.web.infrastructure")
//@EnableWebMvc
public class WebAppConfig {
    
    @Autowired
    private TweetService tweetService;

    @Bean("/hello")
    public MyController hello() {
        return new HelloControler();
    }

    @Bean
    public HandlerMapping hm() {
        //org.springframework.web.servlet.handler.BeanNameUrlHandlerMapping
        return new BeanNameURLHandlerMapping();
    }
    
    @Bean
    public BeanNameUrlHandlerMapping springHM() {
        //org.springframework.web.servlet.handler.BeanNameUrlHandlerMapping
        return new BeanNameUrlHandlerMapping();
    }

    @Bean("/tweets")
    public TweetController tweetController(TweetService tweetService) {
        return new TweetController(tweetService);
    }

}
