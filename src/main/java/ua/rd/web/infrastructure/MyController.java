package ua.rd.web.infrastructure;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author andrii
 */
public interface MyController {
    void handleRequest(HttpServletRequest request, HttpServletResponse response) throws IOException;
}
