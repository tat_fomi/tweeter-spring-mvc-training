package ua.rd.web.infrastructure;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DispatcherServlet extends HttpServlet {

    private AnnotationConfigApplicationContext webContext;

    @Override
    public void init() throws ServletException {

        ConfigurableApplicationContext rootContext
                = (ConfigurableApplicationContext) getServletContext().getAttribute("rootSpringContext");

        String webConfigLocation = getInitParameter("contextConfigLocation");
        System.out.println(webConfigLocation);
        Class<?> webConfigClass = null;
        try {
            webConfigClass = Class.forName(webConfigLocation);
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException(ex);
        }

        webContext = new AnnotationConfigApplicationContext();
        webContext.register(webConfigClass);
        webContext.setParent(rootContext);
        webContext.refresh();

    }

    @Override
    public void destroy() {
        webContext.close();
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HandlerMapping handlerMapping = webContext.getBean(HandlerMapping.class);
        String controllerBeanName = handlerMapping.beanNameFromRequest(req);

        MyController mc = webContext.getBean(controllerBeanName, MyController.class);
        mc.handleRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }
}
