package ua.rd.web.infrastructure;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.BeanNameAware;


public class HelloControler implements MyController, BeanNameAware {
    
    private String beanName;

    @Override
    public void handleRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
         try (PrintWriter out = response.getWriter()) {
            out.print("Hello Controller! " + beanName);
        }
    }

    @Override
    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

}
