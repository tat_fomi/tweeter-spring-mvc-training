package ua.rd.web.infrastructure;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.springframework.beans.BeansException;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class RootSpringContextCreationServeltContextListener implements ServletContextListener {

    private ConfigurableApplicationContext[] springContexts;
        
    @Override
    public void contextInitialized(ServletContextEvent sce) {        
        String[] rootContexts = getRootContextsNames(sce.getServletContext());
        springContexts = createRootSpringContexts(rootContexts);
        sce.getServletContext().setAttribute("rootSpringContext", springContexts[springContexts.length - 1]);
    }   
    
    private String[] getRootContextsNames(ServletContext sc) {
        String contexts = sc.getInitParameter("contextConfigLocation");
        String[] contextsNames = contexts.split(" ");
        return contextsNames;
    }
    
    private ConfigurableApplicationContext[] createRootSpringContexts(String[] contexts) throws BeansException {
        ConfigurableApplicationContext[] applicationContexts
                = new ConfigurableApplicationContext[contexts.length];

        for (int i = 0; i < applicationContexts.length; i++) {
            ConfigurableApplicationContext context;
            if (i == 0) {
                context = new ClassPathXmlApplicationContext(contexts[i]);
            } else {
                context = new ClassPathXmlApplicationContext(
                        new String[]{contexts[i]},
                        applicationContexts[i - 1]);
            }
            applicationContexts[i] = context;
        }
        return applicationContexts;
    }
    
    

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        for (int i = springContexts.length - 1; i >= 0; i--) {
            springContexts[i].close();
        }
    }
    

}
