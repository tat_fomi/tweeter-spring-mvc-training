package ua.rd.web.infrastructure;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import ua.rd.service.TweetService;


/**
 *
 * @author andrii
 */
public class TweetController implements MyController {
    
    private final TweetService tweetService;
    
    public TweetController(TweetService tweetService) {
        this.tweetService = tweetService;
    }

    @Override
    public void handleRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {

        try (PrintWriter out = response.getWriter()) {
            out.println("<b>Hello from Tweet Controller!</b></br>");
            tweetService.getAllTweets().forEach(tw -> out.println("<l>"+ tw.toString() +"</l></br>"));                        
        }
    }
    
}
