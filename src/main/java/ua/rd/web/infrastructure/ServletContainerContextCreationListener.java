package ua.rd.web.infrastructure;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


public class ServletContainerContextCreationListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        String contextConfigLocation = sce.getServletContext().getInitParameter("contextConfigLocation");
        System.out.println(contextConfigLocation);
        sce.getServletContext().setAttribute(contextConfigLocation, sce);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        
    }

}
