package ua.rd.web.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ua.rd.domain.Tweet;

import ua.rd.service.TweetService;


/**
 *
 * @author andrii
 */
@Controller
public class TweetController {
    
    private final TweetService tweetService;
    
    @Autowired
    public TweetController(TweetService tweetService) {
        this.tweetService = tweetService;
    }

    @RequestMapping(value = "/tweets", method = RequestMethod.GET)
    public String tweets(Model model) {
        Iterable<Tweet> tweets = tweetService.getAllTweets();
        model.addAttribute("tweets", tweets);
        return "tweets";
    }
    
}
