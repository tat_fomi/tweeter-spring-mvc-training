package ua.rd.ioc;

public interface BeanDefenition {

    public String getBeanName();

    public <T> Class<T> getType();

}
