package ua.rd.ioc;

import java.util.Map;

public class ExtendedJavaConfig implements Config {

    final BeanDefinition[] beanDefinitions;

    public ExtendedJavaConfig() {
        beanDefinitions = new BeanDefinition[]{};
    }

    private SimpleBeanDefinition mapper(Map.Entry<String, Map<String, Object>> entry) {
        String beanName = entry.getKey();
        Class<?> type = (Class<?>) entry.getValue().get("type");
        Boolean isPrototype =  (Boolean) entry.getValue().get("isPrototype");
        return new SimpleBeanDefinition(beanName, type, isPrototype);
    }
    
    public ExtendedJavaConfig(Map<String, Map<String, Object>> extendedBeanDescriptions) {
        beanDefinitions = extendedBeanDescriptions.entrySet().stream()
                .map(this::mapper)
                .toArray(BeanDefinition[]::new);
    }

    @Override
    public BeanDefinition[] getBeanDefinitions() {
        return beanDefinitions;
    }

}
