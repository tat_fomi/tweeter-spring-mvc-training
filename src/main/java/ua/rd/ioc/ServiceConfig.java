package ua.rd.ioc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import ua.rd.domain.Tweet;
import ua.rd.repository.InMemTweetRepository;
import ua.rd.repository.TweetRepository;
import ua.rd.service.SimpleTweetService;
import ua.rd.service.TweetService;

@Configuration
@Import(RepoConfig.class)
public class ServiceConfig {

    @Autowired
    private Environment env;

    @Bean
    public TweetService tweetService(TweetRepository tr) {
        return new SimpleTweetService(tr) {
            @Override
            public Tweet createNewTweet() {
                return generateTweet();
            }
        };
    }

    @Bean
    @Scope("prototype")
    public Tweet generateTweet() {
        return new Tweet();
    }

    @Bean
    @Scope("prototype")
    @Profile("test")
    public Tweet tweetTest() {
        System.out.println("default tweet");
        return new Tweet("default", "default");
    }
}
