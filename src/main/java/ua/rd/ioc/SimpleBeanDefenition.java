package ua.rd.ioc;

public class SimpleBeanDefenition implements BeanDefenition {

    private final String beanName;
    private final Class<?> type;

    public SimpleBeanDefenition(String beanName, Class<?> type) {
        this.beanName = beanName;
        this.type = type;
    }
    
    @Override
    public String getBeanName() {
        return beanName;
    }

    @Override
    public <T> Class<T> getType() {
        return (Class<T>) type;
    }

}
