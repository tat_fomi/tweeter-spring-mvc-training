package ua.rd.ioc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ua.rd.repository.InMemTweetRepository;
import ua.rd.repository.TweetRepository;
import ua.rd.service.SimpleTweetService;
import ua.rd.service.TweetService;


@Configuration
public class RepoConfig {

    @Bean
    public TweetRepository tweetRepository() {
        return new InMemTweetRepository();
    }

}
