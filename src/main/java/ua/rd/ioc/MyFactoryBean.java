package ua.rd.ioc;

import org.springframework.beans.factory.FactoryBean;
import ua.rd.domain.Tweet;


public class MyFactoryBean implements FactoryBean<Tweet>{

    @Override
    public Tweet getObject() throws Exception {
        return new Tweet();
    }

    @Override
    public Class<?> getObjectType() {
        return Tweet.class;
    }

    @Override
    public boolean isSingleton() {
        return false;
    }

}
