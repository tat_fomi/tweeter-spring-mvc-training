package ua.rd.ioc;

import java.util.Arrays;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class BeanBenchmarkPP implements BeanPostProcessor{

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println(beanName);
        
        if("tweetService".equals(beanName)) {
            System.out.println("Name " + bean);            
            Arrays.asList(bean.getClass().getInterfaces()).forEach(m -> System.out.println(m.getName()));
            System.out.println(bean.getClass().getSuperclass());   
            System.out.println("-----");
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        //System.out.println(beanName);
        return bean;
    }



}
