package ua.rd.service;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ua.rd.domain.Tweet;
import ua.rd.ioc.Benchmark;
import ua.rd.repository.TweetRepository;


@Service("tweetService")
public abstract class SimpleTweetService implements TweetService{
       
    private TweetRepository tweetRepository;   
       
    @Autowired
    //@Qualifier("tweetRepository1")
    public void setTweetRepository(TweetRepository tweetRepository) {
        this.tweetRepository = tweetRepository;
    }
 

    public SimpleTweetService(TweetRepository tweetRepository) {
        this.tweetRepository = tweetRepository;
    }    

    @Override
    public Iterable<Tweet> getAllTweets() {
        return tweetRepository.getAllTweets();
    }
    
    @Benchmark
    public Tweet newTweet(String user, String msg) {
        Tweet tweet = createNewTweet();
        tweet.setUser(user);
        tweet.setMsg(msg);
        tweetRepository.save(tweet);
        return tweet;
    }

    @Lookup
    public abstract Tweet createNewTweet(); 
}
