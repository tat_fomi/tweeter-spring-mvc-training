package ua.rd;

import java.util.Arrays;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import ua.rd.domain.Tweet;
import ua.rd.domain.User;

@Configuration()
//@ComponentScan()
public class AppConfig {

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx
                = new AnnotationConfigApplicationContext(AppConfig.class);

        System.out.println(Arrays.toString(ctx.getBeanDefinitionNames()));
        System.out.println(ctx.getBean("appConfig"));

    }

    @Bean
    public Tweet tweet() {
        System.out.println("Tweet created");
        return new Tweet();
    }

    @Bean
    public User user1(Tweet tweet) {
        return new User(tweet);
    }

    @Bean
    public User user2() {
        return new User(tweet());
    }

}
