package ua.rd;

import java.util.Arrays;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import ua.rd.domain.Tweet;
import ua.rd.ioc.ApplicationContext;
import ua.rd.repository.TweetRepository;
import ua.rd.service.TweetService;


public class SpringConfigRunner {
    public static void main(String[] args) {
               
        ConfigurableApplicationContext repoContext =
               new AnnotationConfigApplicationContext(RepositoryAppConfig.class);

        AnnotationConfigApplicationContext serviceContext =
                new AnnotationConfigApplicationContext();
        serviceContext.register(ServiceAppConfig.class);
        serviceContext.setParent(repoContext);
        
        ConfigurableEnvironment env = serviceContext.getEnvironment();
        env.setActiveProfiles("test");        
        serviceContext.refresh();
        
         System.out.println(
                Arrays.toString(repoContext.getBeanDefinitionNames())
        );
        
        TweetRepository repository = (TweetRepository) repoContext.getBean("tweetRepository");
        System.out.println(repository.getAllTweets());
        
        System.out.println(repoContext.getBeanFactory().getBeanDefinition("tweetRepository"));
        
        
        TweetService ts = (TweetService) serviceContext.getBean("tweetService");
        System.out.println(ts.getAllTweets());
        System.out.println(ts.getClass());

        Tweet t1 = ts.newTweet("Andrri", "Hi");
        Tweet t2 = ts.newTweet("Serg", "Hello");
        System.out.println(t1 == t2);
    }
}
