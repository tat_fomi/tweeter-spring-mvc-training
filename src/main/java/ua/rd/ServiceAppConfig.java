package ua.rd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import ua.rd.domain.Tweet;
import ua.rd.repository.InMemTweetRepository;
import ua.rd.repository.TweetRepository;
import ua.rd.service.SimpleTweetService;
import ua.rd.service.TweetService;

@Configuration
public class ServiceAppConfig {
    
    @Autowired
    private Environment e;
    
    
    @Bean(initMethod = "init")
    @Scope("prototype") 
    @Profile("!test")
    public Tweet tweet() {
        return new Tweet();
    }
    
    @Bean(name = "tweet")
    @Scope("prototype")
    @Profile("test")
    public Tweet tweetTest() {
        System.out.println("Default tweet");
        return new Tweet("default", "default");
    }
    
    @Bean()
    public TweetService tweetService(TweetRepository tr) {
        return new SimpleTweetService(tr) {
            @Override
            public Tweet createNewTweet() {  
                //System.out.println(e.getActiveProfiles()[0]);
                return tweet();
            };
        };
    } 

}
