package ua.rd;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Supplier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ua.rd.domain.Tweet;

import ua.rd.repository.TweetRepository;
import ua.rd.service.TweetService;

public class SpringIoCRunner {

    public static void main(String[] args) {

        ConfigurableApplicationContext repoContext
                = new ClassPathXmlApplicationContext(
                        "repoContext.xml"
                );

        ConfigurableApplicationContext serviceContext
                = new ClassPathXmlApplicationContext(
                        new String[]{"serviceContext.xml"},
                        repoContext
                );
        System.out.println(
                Arrays.toString(repoContext.getBeanDefinitionNames())
        );
        
       
        System.out.println(
                Arrays.toString(serviceContext.getBeanDefinitionNames())
        );
        System.out.println(
                serviceContext.getBeanDefinitionNames().length
        );

        
        TweetService ts = (TweetService) serviceContext.getBean("tweetService");
        System.out.println(ts.getAllTweets());
        System.out.println(ts.getClass());
        System.out.println(
                Arrays.asList(ts.getClass().getMethods())
        );

        Tweet t1 = ts.newTweet("Andrri", "Hi");
        Tweet t2 = ts.newTweet("Serg", "Hello");
        System.out.println(t1 == t2);

        serviceContext.close();
        repoContext.close();
    }
}
