package ua.rd.domain;

import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component
@Scope("singleton")
public class Tweet {
    private String user;
    private String msg;

    public void init() {
        System.out.println("Tweet init");
    }
    
    public Tweet() {
    }   
    
    public Tweet(String user, String msg) {
        this.user = user;
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "Tweet{" + "user=" + user + ", msg=" + msg + '}';
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getUser() {
        return user;
    }

    public String getMsg() {
        return msg;
    }
    
    
    
    
    
}
