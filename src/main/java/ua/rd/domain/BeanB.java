package ua.rd.domain;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;



public class BeanB implements ApplicationContextAware {
    private BeanA beanA;
    private ApplicationContext context;

    public BeanB() {
    }
    
//    public BeanB(BeanA beanA) {
//        this.beanA = beanA;
//        System.out.println(beanA.getClass());
//        System.out.println(beanA.state);
//    

    public void init() {
        beanA = context.getBean("beanA", BeanA.class);
        System.out.println(beanA);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    
    
}
