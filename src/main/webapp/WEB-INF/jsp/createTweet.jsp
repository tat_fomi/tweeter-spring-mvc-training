<%-- 
    Document   : createUser
    Created on : Apr 14, 2017, 4:40:09 PM
    Author     : Andrii_Rodionov
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create Tweet Page</title>
    </head>
    <body>
        <h1>Create/Update Tweet</h1>

        <form method="POST" action="newTweet">
            Name <input type="text" name="name"/>
            Text <input type="text" name="tweet"/>
            <input type="submit" value="Create"/>    
        </form>

    </body>
</html>
