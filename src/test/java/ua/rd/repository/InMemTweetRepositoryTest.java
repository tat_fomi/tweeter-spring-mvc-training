/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.rd.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Repeat;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import ua.rd.RepositoryAppConfig;
import ua.rd.ServiceAppConfig;
import ua.rd.domain.Tweet;
import ua.rd.service.TweetService;

/**
 *
 * @author Andrii_Rodionov
 */

@RunWith(SpringRunner.class)
//@ContextConfiguration(classes = RepositoryAppConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@ContextHierarchy({
        @ContextConfiguration(classes = RepositoryAppConfig.class),
        @ContextConfiguration(classes = ServiceAppConfig.class)
})
@ActiveProfiles("test")
public class InMemTweetRepositoryTest {
        
    @Autowired
    private TweetService ts;
//    @Autowired
//    private Environment e;
    
    public InMemTweetRepositoryTest() {
        System.out.println("Created test");
        //System.out.println(e.getActiveProfiles()[0]);
    }

    @Test
    @Repeat(5)
    public void testInit() {
        Tweet tweet = ts.newTweet("", "");
        System.out.println(tweet);
    }

    @Test
    public void testGetAllTweets() {
        System.out.println(ts);
    }
    
}
